//
//  InputMethodEvaluationSuiteTests.swift
//  InputMethodEvaluationSuiteTests
//
//  Created by Marcel Hansemann on 27/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import XCTest
@testable import InputMethodEvaluationSuite


class InputMethodEvaluationSuiteTests: XCTestCase {
    
    private var session : Session!
    private var round : Round!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let tolerance : Double = 0.01 // allow floating-point values to diverge up to this tolerance level
        
        session = Session(sessionName: "UnitTestSession")
        
        round = Round(withConfiguration: RoundConfiguration(), strings: ["Test"])
        
        
        
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testKSPC() {
        // Keystrokes per character
        let page53  = Phrase("the quick brown fox")
        page53.start()
        page53.appendInputStream("tha<e p<quik<ck brwn<<own t<fox")
        page53.finish(withTranscribed: "the quick brown fox")
        
        XCTAssert(page53.metrics.keystrokesPerCharacter.isEqualWithinToleranceTo(comparisonValue: 1.63))
    }
    func testMsd() {
        let page54_1 = Phrase("the quick brown fox")
        page54_1.start()
        page54_1.finish(withTranscribed: "tha quck browzn fox")
        
        let page54_2 = Phrase("quickly")
        page54_2.start()
        page54_2.finish(withTranscribed: "qucehkly")
        
        XCTAssert(page54_1.metrics.minimumStringDistance == 3)
        XCTAssert(page54_2.metrics.minimumStringDistance == 3)
        XCTAssert(page54_2.metrics.msdErrorRate.isEqualWithinToleranceTo(comparisonValue:0.375))
        
    }
    func testErrorRates() {
       
        let page55 = Phrase("the quick brown fox")
        page55.start()
        page55.finish(withTranscribed: "tha quck browzn fox")
        
        XCTAssert(page55.metrics.incorrectNotFixed == page55.metrics.minimumStringDistance)
        XCTAssert(page55.metrics.correctKeystrokes == max(page55.presented.length, page55.transcribed!.length) - page55.metrics.minimumStringDistance)
        
    }
    func testErrorCharacterClasses() {
        // Tested: correct keystrokes, fixes, incorrect not fixed, incorrect fixed; corrected error rate, uncorrected error rate, total error rate; correction efficiency
        let page55  = Phrase("the quick brown")
        page55.start()
        page55.appendInputStream("th quix<ck brpown")
        page55.finish(withTranscribed: "th quick brpown")
        
        XCTAssert(page55.metrics.correctKeystrokes == 13)
        XCTAssert(page55.metrics.fixes == 1)
        XCTAssert(page55.metrics.incorrectNotFixed == 2)
        XCTAssert(page55.metrics.incorrectFixed == 1)
        
        XCTAssert(page55.metrics.correctedErrorRate == ( 1 / 16 ))
        XCTAssert(page55.metrics.uncorrectedErrorRate == ( 2 / 16 ))
        XCTAssert(page55.metrics.totalErrorRate == ( 2 / 16 ) + ( 1 / 16 ))
        XCTAssert(page55.metrics.correctionEfficiency == 1)
        XCTAssert(page55.metrics.participantConscientiousness == (1 / 3))
        XCTAssert(page55.metrics.utilizedBandwidth == (13 / (13 + 2 + 1 + 1)))
        XCTAssert(page55.metrics.wastedBandwidth == ((2 + 1 + 1) / (13 + 2 + 1 + 1)))
        
    }
    func testCorrectionSequencesAndCostPerCorrection() {
        // Tested: correction sequences, cost per correction
        let page60_1  = Phrase("quick")
        page60_1.recordStateless(transcribed: "quick", inputStream: "pui<<<quick", time: 1.0)
        
        let page60_2  = Phrase("quick")
        page60_2.recordStateless(transcribed: "quick", inputStream: "p<qr<r<uick", time: 1.0)
        
        let page60_3  = Phrase("quick")
        page60_3.recordStateless(transcribed: "quick", inputStream: "p<qv<uj<ick", time: 1.0)
        
        
        guard page60_1.correctionSequences != nil, page60_2.correctionSequences != nil, page60_3.correctionSequences != nil else { XCTAssert(false); return }
        
        XCTAssert(page60_1.correctionSequences! == ["pui<<<"])
        XCTAssert(page60_2.correctionSequences! == ["p<", "r<r<"])
        XCTAssert(page60_3.correctionSequences! == ["p<", "v<", "j<"])
        
        
        XCTAssert(page60_1.metrics.costPerCorrection.isEqualWithinToleranceTo(comparisonValue: 6))
        XCTAssert(page60_2.metrics.costPerCorrection.isEqualWithinToleranceTo(comparisonValue: 3))
        XCTAssert(page60_3.metrics.costPerCorrection.isEqualWithinToleranceTo(comparisonValue: 2))
        
        
        
    }
    
}
fileprivate extension Double {
    public func isEqualWithinToleranceTo(comparisonValue : Double, tolerance: Double = 0.01) -> Bool {
        return comparisonValue-tolerance < self && self < comparisonValue+tolerance
    }
}
