//
//  SessionControllerWrapper.swift
//  InputMethodEvaluationSuite
//
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//
//  This class only exists to facilitate binding from the SessionController (a singleton) to user interface elements in Interface Builder.

import UIKit
/// A wrapper around the SessionController that can be used with Interface Builder / Storyboard (see Readme).
public class SessionControllerWrapper : NSObject, UITextViewDelegate {
    
    private var controller : SessionController {
        return SessionController.sharedInstance
    }
    @IBOutlet var presentedTextView : UITextView? {
        set(value) {
            controller.presentedTextView = value
        }
        get {
            return controller.presentedTextView
        }
    }
    
    @IBOutlet var transcribedTextView : UITextView? {
        set(value) {
            controller.transcribedTextView = value
            
        }
        get {
            return controller.transcribedTextView
        }
    }
   
    
    
    @IBAction  func showNextPhrase(_ sender: Any) {
        controller.showNextPhrase()
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return controller.textView(textView, shouldChangeTextIn: range, replacementText: text)
    }
}
