//
//  DetailViewController.swift
//  IMESDemoApp
//
//  Created by Marcel Hansemann on 24/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
import InputMethodEvaluationSuite
class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!


    func configureView() {
        // Update the user interface for the detail item.
        if let archivedSession : Session = self.archivedSession {
            if let sessionDescriptionTextView = self.sessionDescriptionTextView {
                sessionDescriptionTextView.text = archivedSession.description(usePrecomputedValues:true)
            }
        }
    }

    @IBAction func copyToClipboard(_ sender: Any) {
        UIPasteboard.general.string = sessionDescriptionTextView.text
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        configureView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var archivedSession: Session? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    @IBOutlet weak var sessionDescriptionTextView: UITextView!

}

