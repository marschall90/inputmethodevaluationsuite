//
//  ChoosePhrasePresentationStyleViewController.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 27/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
import InputMethodEvaluationSuite
public class ChoosePhrasePresentationStyleViewController : UITableViewController {
    
    public var chosenPresentationStyle : RoundConfiguration.PhrasePresentationOrder = .originalOrder
    private let options : [RoundConfiguration.PhrasePresentationOrder] = [.originalOrder, .random, .alphabetic]
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel!.text = options[indexPath.row].rawValue
        return cell
    }
    override public func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        self.chosenPresentationStyle = options[indexPath.row]
        
        return indexPath
    }

}
