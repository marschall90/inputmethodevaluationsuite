//
//  MasterViewController.swift
//  IMESDemoApp
//
//  Created by Marcel Hansemann on 24/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
import InputMethodEvaluationSuite
class ArchivedSessionsViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var archivedSessions = [Session]()

    //MARK: Archiving
    
    static let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let archiveURL = documentsDirectory.appendingPathComponent("IMESSessions")
    
    public func saveSessionsFlat() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(archivedSessions, toFile: ArchivedSessionsViewController.archiveURL.path)
        if isSuccessfulSave {
            print("Successfully saved")
        } else {
            print("Saving was unsuccessful")
        }
    }
    public func loadSessions() -> [Session]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: ArchivedSessionsViewController.archiveURL.path) as? [Session]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.archivedSessionsViewController = self
        }
        
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.leftBarButtonItem = editButtonItem


        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        if let loadedSessions = loadSessions() {
            archivedSessions += loadedSessions
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        //clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let session = archivedSessions[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.archivedSession = session
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return archivedSessions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let session : Session = self.archivedSessions[indexPath.row]
        cell.textLabel!.text = session.sessionName
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.archivedSessions.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    @IBAction func unwindToSessionTableView(segue: UIStoryboardSegue) {
        
    }
    @IBAction func cancelSession(segue: UIStoryboardSegue) {
        SessionController.sharedInstance.session = nil
    }
    @IBAction func archiveSession(segue: UIStoryboardSegue) {
        // save the current session, archive it
        if let session = SessionController.sharedInstance.session {
            
            session.archive() // Call this method to make sure the state of all rounds, phrases etc. in this session is "finished" or "cancel" (it's tinned).
            
            archivedSessions.append(session)
            SessionController.sharedInstance.session = nil
            tableView.reloadData()
        }
    }

    
}

