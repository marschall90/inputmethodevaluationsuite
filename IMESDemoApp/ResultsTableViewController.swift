//
//  ResultsTableViewController.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 27/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
public class ResultsTableViewController: UITableViewController {
    public override func viewDidLoad() {
        self.navigationItem.hidesBackButton = true
    }
    @IBAction func doneButtonPressed(_ sender: Any) {
        print("DONE")
        performSegue(withIdentifier: "UnwindToArchivedSessionsList", sender: self)
        
    }
}
