//
//  CountdownView.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 30/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
import QuartzCore
public class CountdownView : UIView {
    private var countLabel : UILabel!
    private var _count : Int = 3
    public var count : Int {
        get { return _count }
        set(value) {
            
            _count = value
            countLabel.text = String(_count)
            
        }
    }
    private(set) var maxCount = 3
    public override init(frame: CGRect) {
        super.init(frame: frame)
        addCountLabel()
        self.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
        self.isOpaque = false
        self.layer.cornerRadius = 20.0
        self.layer.masksToBounds = true
    }
    public convenience init(frame: CGRect, countdownFrom:Int) {
        self.init(frame: frame)
        self.maxCount = countdownFrom
        self.count = countdownFrom
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addCountLabel()
    }
    public func startCountdown(whenFinished:@escaping (() -> ())) {
        let _ : Timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {
            (timer:Timer) in
            let newCount = self.count - 1
            if newCount == 0 {
                timer.invalidate()
                whenFinished()
                self.removeFromSuperview()
            }
            else {
                self.count = newCount
            }
        })
        
    }
    private func addCountLabel() {
        
        countLabel = UILabel(frame: CGRect(x:30,y:30,width:self.frame.width-30,height:self.frame.height-30))
        countLabel.adjustsFontSizeToFitWidth = true
        countLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        countLabel.textColor = UIColor.black
        countLabel.textAlignment = .center
        self.addSubview(countLabel)
        countLabel.center = CGPoint(x: self.frame.size.width / 2, y: self.frame.size.height / 2)
        countLabel.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin,
            .flexibleTopMargin ,.flexibleBottomMargin]
        self.bringSubview(toFront: countLabel)
        
    }
}
