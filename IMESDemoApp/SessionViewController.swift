//
//  SessionViewController.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 24/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
import InputMethodEvaluationSuite
public class SessionViewController : UIViewController, SessionControllerDelegate {
    public var round : Round!
    public var incomingSegue : UIStoryboardSegue?
    
    public override func viewWillAppear(_ animated: Bool) {
        nextButton.title = "Next"
        
        // Hide the textfields before the session starts
        presentedTextview.alpha = 0
        transcribedTextview.alpha = 0
        nextButton.isEnabled = false
    }
    public override func viewDidAppear(_ animated: Bool) {
       
        round.start()
        SessionController.sharedInstance.delegate = self
        SessionController.sharedInstance.applyRoundConfigurationToTextView(transcribedTextview) // apply the round configuration for the first view (only necessary for the first phrase -- this will be done automatically from here on by the session controller)
        presentedTextview.text = round.currentPhrase!.presented
        transcribedTextview.text = ""
        transcribedTextview.becomeFirstResponder()
        
        // Show a countdown view (counting down from 3)
        let countdownViewSize = CGSize(width: 100, height: 100)
        let countdownView = CountdownView(frame:
            CGRect(x: self.view.frame.width / 2 - countdownViewSize.width / 2,
                   y: self.view.frame.height / 3 - countdownViewSize.height / 2,
                   width: countdownViewSize.width,
                   height: countdownViewSize.height
            ), countdownFrom:3)
        self.view.addSubview(countdownView)
        self.view.bringSubview(toFront: countdownView)
        
        countdownView.startCountdown {
            self.transcribedTextview.text = ""
            
            // show again
            self.presentedTextview.alpha = 1
            self.transcribedTextview.alpha = 1
            self.nextButton.isEnabled = true
            
            self.round.currentPhrase!.start()
        }
        
    }
    public override func viewWillDisappear(_ animated: Bool) {
        SessionController.sharedInstance.delegate = nil
    }
    @IBOutlet weak var presentedTextview: UITextView!
    @IBOutlet weak var transcribedTextview: UITextView!
    
    
    // MARK: - SessionControllerDelegate
    public func showingPhrase(atIndex: Int, round: Round) {
        guard round === self.round else { return }
        
        self.navigationItem.title = "\(atIndex+1)/\(round.phrases.count)"
        
        if atIndex == round.phrases.count - 1 {
            // final phrase -- change caption of "Next" button
            nextButton.title = "Done"
            
        }
    }
    public func didShowLastPhraseInRound(round: Round) {
        guard round === self.round else { return }
        
        
        round.finish()
        
        if SessionController.sharedInstance.session!.areAllRoundsFinished {
            //SessionController.sharedInstance.session!.precomputeResults()
            DispatchQueue.main.async() { () -> Void in
                self.showSpinner()
                self.performSegue(withIdentifier: "ShowResults", sender: self)
            }
            
        }
        else {
            
            self.performSegue(withIdentifier: "UnwindToSessionPreparation", sender: self)
        }
        
    }
    private func showSpinner() {
        let spinnerViewSize = CGSize(width: 100, height: 100)
        let spinnerView = SuperimposedSpinnerView(frame:
            CGRect(x: self.view.frame.width / 2 - spinnerViewSize.width / 2,
                   y: self.view.frame.height / 3 - spinnerViewSize.height / 2,
                   width: spinnerViewSize.width,
                   height: spinnerViewSize.height
        ))
        self.view.addSubview(spinnerView)
        self.view.bringSubview(toFront: spinnerView)
    }
    @IBOutlet weak var nextButton: UIBarButtonItem!
    
}
