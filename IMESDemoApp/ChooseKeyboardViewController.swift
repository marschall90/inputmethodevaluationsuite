//
//  ChooseKeyboardViewController.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 24/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
import InputMethodEvaluationSuite
public class ChooseKeyboardViewController : UITableViewController {
    public var chosenKeyboard : String?
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return KeyboardManager.installedKeyboards.count
    }
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel!.text = KeyboardManager.installedKeyboards[indexPath.row]
        return cell
    }
    override public func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        self.chosenKeyboard = KeyboardManager.installedKeyboards[indexPath.row]
        
        return indexPath
    }
}
