//
//  EditRoundsTableViewController.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 24/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
import InputMethodEvaluationSuite
public class EditRoundsTableViewController : UITableViewController {
    public var session : InputMethodEvaluationSuite.Session!
    public var showNewRoundDialogIfNoRoundsConfigured : Bool = false
    @IBOutlet weak var addRoundButton: UIBarButtonItem!
    public override func viewDidAppear(_ animated: Bool) {
        if showNewRoundDialogIfNoRoundsConfigured && session.rounds.count == 0 {
            // no rounds yet -- immediately show new session dialogue
            showNewRoundDialogIfNoRoundsConfigured = false
            self.performSegue(withIdentifier: "NewRoundSettings", sender: self)
        }
    }
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "NewRoundSettings"/*, let from = sender as? UIBarButtonItem, from === addRoundButton*/ {
            
            
            if let nav = segue.destination as? UINavigationController, let roundSettingsViewController = nav.viewControllers.first as? RoundSettingsViewController {
                roundSettingsViewController.roundConfiguration = nil
            }
        }
        else if segue.identifier == "ExistingRoundSettings", sender is UITableViewCell {
            // edit existing round
            if let roundSettingsViewController = segue.destination as? RoundSettingsViewController {
                roundSettingsViewController.roundConfiguration = session.rounds[self.tableView.indexPathForSelectedRow!.row].configuration
            }
            
        }
        
    }
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return session.rounds.count
    }
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel!.text = "Round \(String(indexPath.row+1))"
        return cell
    }
    @IBAction func editedRoundConfiguration(segue: UIStoryboardSegue) {
        guard self.tableView.indexPathForSelectedRow != nil else { return } // We need to have a selected round here so we can store the round configuration.
        if segue.source is RoundSettingsViewController {
            // we need to reapply the configuration if we made changes to the phraseset information (order etc.), since these are initialized together with the Round object itself.
            if session.rounds.count > self.tableView.indexPathForSelectedRow!.row {
                let selectedSession = session.rounds[self.tableView.indexPathForSelectedRow!.row]
                selectedSession.applyConfigurationChanges()
                
            }
        }
        
        
    }
    @IBAction func createdRoundConfiguration(sender:UIStoryboardSegue) {
        // add new round configuration to the list
        if let sourceViewController = sender.source as? RoundSettingsViewController, let roundConfiguration = sourceViewController.roundConfiguration {
            // create new round
            let newRound = Round(withConfiguration: roundConfiguration)
            session.rounds.append(newRound)
            
            // reload data
            self.tableView.reloadData()
        }
    }
    @IBAction func cancelledNewRound(segue: UIStoryboardSegue) {
        // didn't successfully construct a new round (unwind)
    }
}
