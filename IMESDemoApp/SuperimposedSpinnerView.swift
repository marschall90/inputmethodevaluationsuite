//
//  SuperimposedSpinnerView.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 06/05/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
import QuartzCore
public class SuperimposedSpinnerView : UIView {
    private var activityIndicator : UIActivityIndicatorView!
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        addActivityIndicator()
        self.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
        self.isOpaque = false
        self.layer.cornerRadius = 20.0
        self.layer.masksToBounds = true
    }
  
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addActivityIndicator()
    }
    
    private func addActivityIndicator() {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x:30,y:30,width:self.frame.width-30,height:self.frame.height-30))
        activityIndicator.center = CGPoint(x: self.frame.size.width / 2, y: self.frame.size.height / 2)
        activityIndicator.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin,
                                       .flexibleTopMargin ,.flexibleBottomMargin]
        self.bringSubview(toFront: activityIndicator)
        activityIndicator.startAnimating()
    }
}
