//
//  RoundSettingsViewController.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 24/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
import InputMethodEvaluationSuite
public class RoundSettingsViewController : UITableViewController {
    private let multicharacterExplanation = "Enable \"Multicharacter insertion\" for keyboards that might insert multiple letters at a time. This includes gesture-based keyboards."
    private let multicharacterAutocorrectWarning = "⚠️ Unsupported configuration: For autocorrection to work correctly, \"Multicharacter insertion\" must be enabled."
    
    
    public var roundConfiguration : RoundConfiguration?
    public var fromRoundsList : Bool = false

    @IBAction func didCancel() {
        // user pressed cancel button
        self.performSegue(withIdentifier: "UnwindToRoundsList", sender: self)
    }
    
    public override func viewDidLoad() {
    
        if roundConfiguration == nil {
            // create a new round configuration
            roundConfiguration = RoundConfiguration()
            self.navigationItem.rightBarButtonItem = saveButton
            // also add a cancel button. We're presented modally, so there is no back button on the navigation bar.
            let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(didCancel))
            self.navigationItem.leftBarButtonItem = cancelButton
        }
        else {
            // editing an existing configuration -- no save button
            self.navigationItem.rightBarButtonItem = nil
        }
        
        if let value = roundConfiguration {
            
            phraseSetLabel.text = (value.phraseSet == "" ? PhrasesetManager.sharedInstance.availablePhrasesets.first! : value.phraseSet)
            keyboardLabel.text = value.keyboardName
            multicharacterInsertionSwitch.isOn = value.allowsMulticharacterInsertion
            allowAutocompleteSwitch.isOn = value.autocorrectEnabled
            lowercaseOnlySwitch.isOn = value.lowercaseOnly
            alphanumericOnlySwitch.isOn = value.alphanumericOnly
            autocapitalizationSwitch.isOn = value.autocapitalizationEnabled
        }
        
        
        updateSaveButtonState()
    }
    
    // unwind actions
    @IBAction func choseKeyboard(segue: UIStoryboardSegue) {
        if let chooseKeyboardController = segue.source as? ChooseKeyboardViewController, chooseKeyboardController.chosenKeyboard != nil {
            keyboardLabel.text = chooseKeyboardController.chosenKeyboard
        }
        updateSaveButtonState()
    }
    @IBAction func chosePhraseset(segue: UIStoryboardSegue) {
        if let choosePhrasesetController = segue.source as? ChoosePhrasesetViewController, choosePhrasesetController.chosenPhraseset != nil {
            phraseSetLabel.text = choosePhrasesetController.chosenPhraseset
        }
        updateSaveButtonState()
    }
    @IBAction func chosePresentationStyle(segue: UIStoryboardSegue) {
        if let choosePresentationStyleController = segue.source as? ChoosePhrasePresentationStyleViewController, choosePresentationStyleController.chosenPresentationStyle != nil {
            presentationStyleLabel.text = choosePresentationStyleController.chosenPresentationStyle.rawValue
        }
    }
    
    @IBOutlet weak var phraseSetLabel: UILabel!
    
    @IBOutlet weak var keyboardLabel: UILabel!
    
    
    @IBOutlet weak var presentationStyleLabel: UILabel!
    
    @IBOutlet weak var allowAutocompleteSwitch: UISwitch!
    
    @IBOutlet weak var alphanumericOnlySwitch: UISwitch!
    
    @IBOutlet weak var phraseCountLabel: UITextField!
    private func updateSaveButtonState() {
        if saveButton != nil
        {
            saveButton.isEnabled = (keyboardLabel.text != nil && keyboardLabel.text!.length > 0) &&
                                    (phraseSetLabel.text != nil && phraseSetLabel.text!.length > 0)
        }
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let button = sender as? UIBarButtonItem, button === saveButton else { return }
        
        save()
    }
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParentViewController {
            // moving back in the hierachy
            
            save()
        }
    }
    
    @IBAction func phraseCountTextfieldEditingDidEnd(_ sender: Any) {
        if let sender = sender as? UITextField {
            sender.resignFirstResponder()
        }
    }
    private func save() {
        if let roundConfiguration = self.roundConfiguration, phraseSetLabel.text != nil, keyboardLabel.text != nil {
            roundConfiguration.alphanumericOnly = alphanumericOnlySwitch.isOn
            roundConfiguration.autocorrectEnabled = allowAutocompleteSwitch.isOn
            roundConfiguration.allowsMulticharacterInsertion = multicharacterInsertionSwitch.isOn
            roundConfiguration.lowercaseOnly = lowercaseOnlySwitch.isOn
            roundConfiguration.phraseSet = phraseSetLabel.text!
            roundConfiguration.keyboardName = keyboardLabel.text!
            roundConfiguration.autocapitalizationEnabled = autocapitalizationSwitch.isOn
            if let key = presentationStyleLabel.text, let presentationOrder = RoundConfiguration.PhrasePresentationOrder(rawValue: key) {
            
                roundConfiguration.phrasePresentationOrder = presentationOrder
            }
            if let phraseCount = phraseCountLabel.text, let phraseCountInt = Int(phraseCount) {
                // 0 means present all phrases.
                roundConfiguration.phraseCount = phraseCountInt
            }
            
        }

    }
    
    @IBAction func changedAutocorrectSwitch(_ sender: Any) {
        if let sender = sender as? UISwitch, sender === allowAutocompleteSwitch {
            if sender.isOn == true {
                multicharacterInsertionSwitch.isOn = true
            }
            self.tableView.footerView(forSection: 0)!.textLabel!.text = multicharacterExplanation
        }
    }
    @IBAction func changedMulticharacterInsertionSwitch(_ sender: Any) {
        if let sender = sender as? UISwitch, sender === multicharacterInsertionSwitch {
            if sender.isOn == false && allowAutocompleteSwitch.isOn == true {
                self.tableView.footerView(forSection: 0)!.textLabel!.text = multicharacterAutocorrectWarning
            }
            else {
             self.tableView.footerView(forSection: 0)!.textLabel!.text = multicharacterExplanation
            }
            
        }
    }
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var multicharacterInsertionSwitch: UISwitch!
    @IBOutlet weak var autocapitalizationSwitch: UISwitch!
    @IBOutlet weak var lowercaseOnlySwitch: UISwitch!
    @IBAction func dismissKeyboard(_ sender: Any) {
        if let sender = sender as? UIControl {
            sender.resignFirstResponder()
        }
    }
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        dismissKeyboard(phraseCountLabel)
    }
}
