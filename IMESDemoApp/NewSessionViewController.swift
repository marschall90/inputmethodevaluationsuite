//
//  NewSessionViewController.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 24/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
import InputMethodEvaluationSuite

public class NewSessionViewController : UIViewController, UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    @IBOutlet weak var sessionNameField: UITextField!
    @IBAction func editingDidEnd(_ sender: Any) {
        sessionNameField.resignFirstResponder()
    }
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let from = sender as? UIButton, from === setupRoundsButton, let destination = segue.destination as? EditRoundsTableViewController {
            destination.session = session
            destination.showNewRoundDialogIfNoRoundsConfigured = true
        }
        else if segue.identifier != nil, segue.identifier! == "StartNewSession", let targetNvc = segue.destination as? UINavigationController, let target = targetNvc.viewControllers.first as? SessionPreparationViewController {
            
            target.session = self.session
            session.sessionName = sessionNameField.text ?? "New session"
            SessionController.sharedInstance.session = self.session
        }
    }
    @IBOutlet weak var setupRoundsButton: UIButton!
    
    private var _session : Session?
    var session : Session {
        if (_session == nil) {
            _session = Session(sessionName:sessionNameField.text ?? "")
        }
        return _session!
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        
        // check if we have a title, more than one round and are good to go
        if session.rounds.count > 0, sessionNameField.text != nil {
            // TODO must update name to field content, actually best to create the session here?
            startSessionButton.isEnabled = true
        }
        else {
            startSessionButton.isEnabled = false
        }
    }
    @IBOutlet weak var startSessionButton: UIBarButtonItem!
    
 
}
