//
//  SessionPreparationViewController.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 24/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
import InputMethodEvaluationSuite
public class SessionPreparationViewController : UIViewController {
    
    @IBOutlet weak var keyboardNameLabel: UILabel!
    @IBOutlet weak var changeKeyboardPromptView: UIView!
    @IBOutlet weak var secondaryCaptionLabel: UILabel!
    @IBOutlet weak var phaseTitleLabel: UILabel!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var invisibleTextfield: UITextField!
    
    public var session : InputMethodEvaluationSuite.Session!
    
    private func showKeyboard() {
        invisibleTextfield.autocorrectionType = .no
        invisibleTextfield.becomeFirstResponder()
    }
    private func hideKeyboard() {
        invisibleTextfield.resignFirstResponder()
    }
    
    private var originalChangeKeyboardPromptFrame : CGRect? = nil
    private func showKeyboardAndDetect(neededKeyboard: String) {
        guard keyboardNameLabel != nil else { return }
        keyboardNameLabel.text = neededKeyboard
        
        
        
        let _ : Timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: {
            (timer:Timer) in
            
            // check if the user has already activated the needed keyboard
            if let currentKeyboard = KeyboardManager.currentlyDisplayedKeyboard, currentKeyboard == neededKeyboard {
                // user has chosen correct keyboard
                timer.invalidate()
                self.hideKeyboard()
                
                
                UIView.animate(withDuration: 0.5, animations: {
                    
                    self.changeKeyboardPromptView.frame = CGRect(x:self.changeKeyboardPromptView.frame.origin.x, y:self.view.frame.height, width:self.changeKeyboardPromptView.frame.width, height:0)
                    self.changeKeyboardPromptView.alpha = 0.0
                }, completion: {
                    (finished:Bool) in
                    self.changeKeyboardPromptView.isHidden = true
                    self.changeKeyboardPromptView.alpha = 1.0
                    self.changeKeyboardPromptView.frame = self.originalChangeKeyboardPromptFrame!
                })
            }
        })
        
        showKeyboard()

    }
    public override func viewDidLoad() {
        /*originalChangeKeyboardPromptFrame = changeKeyboardPromptView.frame
        
        phaseTitleLabel.text = "Round \(session.roundIndex+1)"
        secondaryCaptionLabel.text = "of \(session.rounds.count)"
        
        guard session.currentRound?.configuration.keyboardName != nil else { return }
        let neededKeyboard = session.currentRound!.configuration.keyboardName
        */
        //showKeyboardAndDetect(neededKeyboard: neededKeyboard)
        
        originalChangeKeyboardPromptFrame = changeKeyboardPromptView.frame
        
    }
    public override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    public override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        guard phaseTitleLabel != nil, secondaryCaptionLabel != nil, changeKeyboardPromptView != nil else { return }
        
        phaseTitleLabel.text = "Round \(session.roundIndex+1)"
        secondaryCaptionLabel.text = "of \(session.rounds.count)"
        changeKeyboardPromptView.frame = originalChangeKeyboardPromptFrame ?? changeKeyboardPromptView.frame
        changeKeyboardPromptView.isHidden = false
        changeKeyboardPromptView.alpha = 1.0

    }
    public override func viewDidAppear(_ animated: Bool) {
        guard session.currentRound?.configuration.keyboardName != nil else { return }
        let neededKeyboard = session.currentRound!.configuration.keyboardName
        
        showKeyboardAndDetect(neededKeyboard: neededKeyboard)
    }
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier != nil, segue.identifier == "StartRound", let target = segue.destination as? SessionViewController {
            if session.state == .ready {
                session.start()
            }
            target.round = session.currentRound
            
        }
    }
    @IBAction func prepareNextRound(segue:UIStoryboardSegue) {
        guard session.state == .inProgress, session.hasNextRound else { return }
        
        let _ = session.nextRound()
        //changeKeyboardPromptView.frame = originalChangeKeyboardPromptFrame
        //changeKeyboardPromptView.isHidden = false
        
    }
    
    @IBAction func startRound(_ sender: Any) {
        performSegue(withIdentifier: "ShowRound", sender: sender)
    }
}
