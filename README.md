# README #

InputMethodEvaluationSuite is a framework to aid in the evaluation of text input methods / keyboards on iOS. Various statistics are captured from user input (cf. "Measures of Text Entry Performance", Jacob O. Wobbroc, 2007). The framework comes with a demo app that can be used to conduct evaluations of input methods by having the study participant transcribe a presented string. 

### How to use the demo app ###

Tap the Add button in the upper right corner to set up a new evaluation session. Add "rounds" (one pass with a specific input method). Rounds have various settings:

* **Keyboard:** Choose the keyboard to use for this evaluation session
* **Phraseset:** Choose the phraseset to transcribe during this session (a .txt file included in the bundle)
* **Presentation order:** Choose the order in which to present the phrases to the user
* **Alphanumeric/lowercase only:** Modify the phrases. Making them lowercase only will always disable autocapitalisation.
* **Phrase count:** Restrict the number of phrases selected from the phraseset. Set to 0 to use all phrases in the set.
* **Autocorrect enabled:** Choose whether to allow autocorrect. *Note:* This setting must be activated when using a keyboard that inserts multiple keystrokes at a time (such as Swype etc.), otherwise the keyboard will not function properly.

Then proceed to the session start. Before each round, the user will be asked to choose the keyboard for this round from the keyboard menu (the globe icon in the lower left corner of the keyboard). [This is because the keyboard cannot be set programmatically.]

After a transcription session, results with metrics for each round are shown. Tapping "Done" takes you back to the main screen, where the session has been saved as an archived session (not persistent). Tap on an archived session to see its results in text form and copy them to the clipboard.

### Integrating the framework ###
Add the framework to your project as a target and change your build settings to include it (also add a "Copy files" build phase so the framework is copied into your app bundle). Import "InputMethodEvaluationSuite" when needed.

The framework includes functionality to integrate it into existing applications / UIs. The following components can be used to integrate the framework:


# SessionController #

This is the main controller for an ongoing session. It is a singleton. Assign it any sessions you are creating and it can take care of state management, supply the next phrase to transcribe etc.

# SessionControllerWrapper #

This is a wrapper around the SessionController that can be used in Interface Builder. When in Interface Builder, drag an NSObject onto the view controller. Select it and set its class to "SessionControllerWrapper". It provides these outlets:

* presentedTextview: The UITextView in which the presented string will be shown.
* transcribedTextview: The UITextView in which the transcribed string will be entered.

It also offers one action method, showNextPhrase. This could be used to hook up a "Next phrase" button (ctrl-drag from the button to the SessionControllerWrapper object in the navigation controller).

# SessionControllerDelegate #
This is a protocol that can be adapted by classes to be notified of session state changes. It offers two methods: 
    
```
#!swift

 func didShowLastPhraseInRound(round: Round)
 func showingPhrase(atIndex:Int, round:Round)
```
didShowLastPhraseInRound can be used to implement changing to the next round / making UI updates. showingPhrase could be used to show the current position within the phrase set, e.g. in a label.
The delegate can be assigned to the shared instance of the SessionController.

# SessionResultsTableViewDataSource #
This is a UITableViewDataSource-compliant class that can be used to easily show session results / metrics in a UITableView. To use it in Interface Builder, just add an NSObject to the view controller, assign it class "SessionResultsTableViewDataSource", and ctrl-drag from the table view's data source outlet to the SessionResultsTableViewDataSource object.