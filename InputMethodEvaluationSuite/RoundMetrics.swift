//
//  RoundMetrics.swift
//  InputMethodEvaluationSuite
//
//  Aggregate metrics for one round.
//
//  Created by Marcel Hansemann on 10/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import Foundation
/// Aggregate metrics for one round.
public class RoundMetrics : KeyValueDescribable, CustomStringConvertible, KeyValueArrayInitializable {
    
    
    public var round : Round!
    public convenience init(round: Round) {
        self.init()
        self.round = round
    }
    public init() {
        // nothing
    }
    public required convenience init(withKeyValueDescriptions: [(key: MetricKey, value: Any)]) {
        self.init()
        var precomp = [(key: MetricKey, value: Any)]()
        for (key, value) in withKeyValueDescriptions {
            precomp.append((key: key, value: value))
        }
        _precomputedKeyValueDescription = precomp
    }
    public static var supportedKeys : [MetricKey] {
        return [.wordsPerMinute, .backspaceCount, .keystrokesPerCharacter, .correctKeystrokes, .incorrectNotFixed, .fixes, .incorrectFixed, .correctedErrorRate, .totalErrorRate, .correctionEfficiency, .participantConscientiousness, .utilizedBandwidth, .wastedBandwidth, .presentedCharCount, .transcribedCharCount, .inputTime, .costPerCorrection, .msdErrorRate, .minimumStringDistance]
    }
    public var keyValueDescription: [(key: MetricKey, value: Any)] {
        _precomputedKeyValueDescription = [(key: .presentedCharCount, value: presentedCharCount),
                (key: .transcribedCharCount, value: transcribedCharCount),
                (key: .inputTime, value: inputTime),
                (key: .wordsPerMinute, value:wordsPerMinute),
                (key: .backspaceCount, value:backspaceCount),
                (key: .keystrokesPerCharacter, value:keystrokesPerCharacter),
                (key: .correctKeystrokes, value:correctKeystrokes),
                (key: .incorrectNotFixed, value:incorrectNotFixed),
                (key: .fixes, value:fixes),
                (key: .incorrectFixed, value:incorrectFixed),
                (key: .correctedErrorRate, value:correctedErrorRate),
                (key: .uncorrectedErrorRate, value:uncorrectedErrorRate),
                (key: .totalErrorRate, value:totalErrorRate),
                (key: .correctionEfficiency, value:correctionEfficiency),
                (key: .participantConscientiousness, value:participantConscientiousness),
                (key: .utilizedBandwidth, value:utilizedBandwidth),
                (key: .wastedBandwidth, value:wastedBandwidth),
                (key: .msdErrorRate, value:msdErrorRate),
                (key: .costPerCorrection, value:costPerCorrection),
                (key: .minimumStringDistance, value:minimumStringDistance)]
        return _precomputedKeyValueDescription!
        
    }
    public var _precomputedKeyValueDescription : [(key: MetricKey, value: Any)]? = nil
    public var precomputedKeyValueDescription : [(key: MetricKey, value: Any)] {
        if (_precomputedKeyValueDescription == nil) {
            _precomputedKeyValueDescription = keyValueDescription
        }
        return _precomputedKeyValueDescription!
    }
    public var description: String {
        return description(usePrecomputedValues:false)
    }
    public func description(usePrecomputedValues:Bool) -> String {
        let keyValueDescription = usePrecomputedValues ? self.precomputedKeyValueDescription : self.keyValueDescription
        return keyValueDescription.map({
            (key:MetricKey, value:Any) -> String in
            return "\(key.rawValue.localized): \(String(describing: value))"
        }).joined(separator: "\n")
    }
    
    public var minimumStringDistance : Double {
        let msd : Double = round.phrases.reduce(0, { sum, phrase in
            sum + Double(phrase.metrics.minimumStringDistance)
        })
        return msd / Double(round.phrases.count)
    }
    public var costPerCorrection : Double {
        let msd : Double = round.phrases.reduce(0, { sum, phrase in
            sum + Double(phrase.metrics.costPerCorrection)
        })
        return msd / Double(round.phrases.count)
    }
    public var msdErrorRate : Double {
        let msd : Double = round.phrases.reduce(0, { sum, phrase in
            sum + Double(phrase.metrics.msdErrorRate)
        })
        return msd / Double(round.phrases.count)
    }
    
    public var presentedCharCount : Int {
        let count = round.phrases.reduce(0, { sum, phrase in
            sum + phrase.presented.length
        })
        return count
    }
    public var transcribedCharCount : Int {
        
        let count = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.transcribed!.length : 0)
        })
        return count
    }
    public var inputTime : TimeInterval {
        
        let count = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.duration : 0)
        })
        return count
    }
    // TODO pause time? total time?
    public var wordsPerMinute : Double {
        let count = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.wordsPerMinute : 0)
        })
        return count / Double(round.phrases.count)
    }
    public var backspaceCount : Int {
        let count = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.backspaceCount : 0)
        })
        return count
    }
    public var keystrokesPerCharacter : Double {
        let count = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.keystrokesPerCharacter : 0)
        })
        return count / Double(round.phrases.count)
    }
    public var correctKeystrokes : Int {
        let count = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.correctKeystrokes : 0)
        })
        return count
    }
    public var incorrectNotFixed : Int {
        let count = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.incorrectNotFixed : 0)
        })
        return count
    }
    public var fixes : Int {
        let count = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.fixes : 0)
        })
        return count
    }
    public var incorrectFixed : Int {
        let count = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.incorrectFixed : 0)
        })
        return count
    }
    public var correctedErrorRate : Double {
        let count : Double = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.correctedErrorRate : 0)
        })
        return count / Double(round.phrases.count)
    }
    public var uncorrectedErrorRate : Double {
        let count: Double = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.uncorrectedErrorRate : 0)
        })
        return count / Double(round.phrases.count)
    }
    public var totalErrorRate : Double {
        let count: Double = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.totalErrorRate : 0)
        })
        return count / Double(round.phrases.count)
    }
    public var correctionEfficiency : Double {
        let count: Double = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.correctionEfficiency : 0)
        })
        return count / Double(round.phrases.count)
    }
    public var participantConscientiousness : Double {
        let count: Double = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.participantConscientiousness : 0)
        })
        return count / Double(round.phrases.count)
    }
    public var utilizedBandwidth : Double {
        let count: Double = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.utilizedBandwidth : 0)
        })
        return count / Double(round.phrases.count)
    }
    public var wastedBandwidth : Double {
        let count: Double = round.phrases.reduce(0, { sum, phrase in
            sum + (phrase.isTranscribed ? phrase.metrics.wastedBandwidth : 0)
        })
        return count / Double(round.phrases.count)
    }
}
