//
//  PhrasesetManager.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 24/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import Foundation
public class PhrasesetManager {
    
    /// The static shared instance of the PhrasesetManager.
    public static let sharedInstance = PhrasesetManager()
    
    private lazy var loadedPhrasesets : [String:[Phrase]] = [String:[Phrase]]()
    
    private var _availablePhrasesets : [String] = ["Default Phraseset"]
    /// The array of available phrasesets (phrasesets that have been announced to the PhrasesetManager and presumably reside in storage).
    public var availablePhrasesets : [String] {
        return _availablePhrasesets
    }
    
    /// Use this function to notify the Phraseset manager of the availability of a certain phrase set.
    /// - Remark: Phrase sets are .txt files in either the framework's or the application's bundle. Using this method will announce availability to the user and result in the framework attempting to find and load such a file.
    /// - Parameter withName: The name of the framework, equivalent to the filename of the `.txt` file (but without the extension).
    public func addAvailablePhraseset(withName:String) {
        // TODO doesn't check if this file is actually found. Will use withName + ".txt" to find file in either the library or the main bundle of the app.
        _availablePhrasesets.append(withName)
    }
    
    /// Returns the phrases that are in the specified phrase set.
    ///
    /// - Parameters:
    ///   - forPhraseset: The name (previously announced by calling `_addAvailablePhraseset`) of the phraseset whose phrases you are retrieving.
    ///   - lowercase: _Optional_ Whether to make these phrases lowercase (default: `false`).
    ///   - alphanumericOnly: _Optional_ Whether to make these phrases alphanumeric only by filtering out special characters (default: `false`).
    /// - Returns: A set of phrases that are in the given phraseset, modified by the indicated flags.
    public func phrases(forPhraseset: String, lowercase:Bool = false, alphanumericOnly:Bool = false) -> [Phrase] {
        if let loadedPhraseset = loadedPhrasesets[forPhraseset] {
            return loadedPhraseset.clone(lowercase: lowercase, alphanumericOnly: alphanumericOnly)
        }
        else {
            var textFilePath : String?
            
            // load from text file
            if let path = Bundle(identifier: "inso.InputMethodEvaluationSuite")?.path(forResource: forPhraseset, ofType: "txt") {
               textFilePath = path
            }
            else if let path = Bundle.main.path(forResource: forPhraseset, ofType: "txt") {
                textFilePath = path
            }
            guard (textFilePath != nil) else { return [Phrase("No phrases loaded")] }
            
            do {
                let data = try String(contentsOfFile: textFilePath!, encoding: .utf8)
                let lines = data.components(separatedBy: .newlines)
                var returnedPhraseSet = [Phrase]()
                for line in lines {
                    
                    // a hash sign # at the start of a line marks a comment, so skip those
                    if line.length > 0 && line.characters.first != "#" { returnedPhraseSet.append(Phrase(line)) }
                }
                
                loadedPhrasesets[forPhraseset] = returnedPhraseSet
                return returnedPhraseSet.clone(lowercase: lowercase, alphanumericOnly: alphanumericOnly)
            } catch {
                print(error)
                return [Phrase("No phrases loaded")]
            }
            
        }
        
    }
    
}
