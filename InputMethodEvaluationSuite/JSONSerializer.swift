//
//  JSONSerializer.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 06/05/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import Foundation
/// As of now unfunctional. Can be used to implement JSON serialization in the future.
public class JSONSerializer {
    public static func serialize(object:KeyValueDescribable) -> String {
        let beginning = "{ ", end = " }"
        var jsonStr = beginning
        let kvd = object.keyValueDescription
        for (key, value) in kvd {
            var valueString = ""
            if let valueArray = value as? [(key:MetricKey, value:Any)] {
                // we have an array here
                valueString += "["
                for vv in valueArray {
                    //valueString += serialize(object: <#T##KeyValueDescribable#>)
                }
                valueString += "]"
            }
            else if let value = value as? String {
                valueString = value
            }
            else if let value = value as? Bool {
                valueString = value ? "true" : "false"
            }
            else if let value = value as? Double {
                valueString = String(value)
            }
            else if let value = value as? Int {
                valueString = String(value)
            }
            
            
        }
        return "" // TODO implement JSON here
    }
}
