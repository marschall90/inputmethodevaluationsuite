//
//  KeyValueDescribable.swift
//  InputMethodEvaluationSuite
//
//  Makes sure a class is able to return an array of key-value tuples that describe its variables or content, e.g. for serialization.
//  Created by Marcel Hansemann on 15/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import Foundation
public protocol KeyValueDescribable {
    var keyValueDescription : [(key: MetricKey, value: Any)] { get }
    static var supportedKeys : [MetricKey] { get }
}
public protocol Precomputable {
    
    func precomputeValues()
    var precomputedKeyValueDescription : [(key: String, value: Any)] { get }
}
public extension Array where Element: KeyValueDescribable {
    public var keyValueDescription:[[(key:MetricKey, value:Any)]]  {
        get {
            var array : [[(key:MetricKey, value:Any)]] = [[(key:MetricKey, value:Any)]]()
            for element in self {
                array.append(element.keyValueDescription)
            }
            return array
        }
    }
    
}
public protocol KeyValueObjectInitializable {
    init(withKeyValueDescription:(key:MetricKey, value:Any))
}
public protocol KeyValueArrayInitializable {
    init(withKeyValueDescriptions:[(key:MetricKey, value:Any)])
}
