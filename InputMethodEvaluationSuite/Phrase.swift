//
//  Phrase.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 10/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import Foundation

/// A phrase that the user is expected to transcribe.
public class Phrase : CustomStringConvertible, KeyValueDescribable, KeyValueArrayInitializable {
    public static var supportedKeys: [MetricKey] {
        return [.presented, .transcribed, .inputStream, .duration]
    }
    public var keyValueDescription: [(key: MetricKey, value: Any)] {
        return [(key: .presented, value:presented), (key: .transcribed, value:transcribed ?? ""), (key: .inputStream, value:inputStream), (key: .duration, value:duration)]
    }
   
    public required convenience init(withKeyValueDescriptions: [(key: MetricKey, value: Any)]) {
        
        var presented : String?, transcribed : String?, inputStream: String?, duration: Double?
        for (key, value) in withKeyValueDescriptions {
            switch key {
            case .presented:
                presented = value as? String ?? ""
            case .transcribed:
                transcribed = value as? String ?? ""
            case .inputStream:
                if let iS = value as? String { inputStream = iS }
            case .duration:
                duration = value as? Double ?? 0
            default:
                break
            }
        }
        if let presented = presented, let transcribed = transcribed, let inputStream = inputStream, let duration = duration {
            self.init(presented:presented)
            self.recordStateless(transcribed: transcribed, inputStream: inputStream, time: duration)
        }
        else { self.init(presented: "(Not loaded)") }
    }
    public var description: String {
       return description(usePrecomputedValues: false)
}
    public func description(usePrecomputedValues: Bool) -> String {
        let str = "Phrase ------ {\nPresented: \(self.presented)\nTranscribed: \(self.transcribed ?? "") \nInput stream: \(self.separatedInputStream)\n\nDuration: \(self.duration)\nMetrics:\n{\(self.metrics.description(usePrecomputedValues:usePrecomputedValues)) \n}\n\n}"
        return str
    }
    private var startTime : Date?
    private var endTime : Date?
    
    private var _time : TimeInterval?
    /// How long it took the user to transcribe the phrase.
    /// - Remark: Is nil when the phrase has not yet been transcribed.
    public var duration : TimeInterval {
        get {
            if _time != nil { return _time! }
            guard startTime != nil, endTime != nil else {
                return 0
            }
            return endTime!.timeIntervalSinceReferenceDate - startTime!.timeIntervalSinceReferenceDate
        }
        set(value) {
            _time = value
        }
        
    }
    
    /// The array of input events related to this phrase (such as keystrokes).
    public lazy var inputEvents = [InputEvent]()
    
    /// Whether the phrase has already been transcribed.
    public var isTranscribed : Bool {
        return self.state == .finished
    }
    
    /// The presented string.
    public var presented : String
    
    /// The transcribed string (transcribed by the user.)
    public var transcribed : String? = nil
    
    public enum PhraseState {
        case ready, inProgress, finished
    }
    public var state : PhraseState
    
    public lazy var metrics : PhraseMetrics = PhraseMetrics(phrase: self)
    
    /// The input stream, which includes backspace characters ("<").
    public var inputStream : String {
        var ret = ""
        for inputEvents in self.inputEvents {
            ret += String(inputEvents.isDeletion ? "<" : inputEvents.inputString)
        }
        return ret
    }
    
    /// The separated input stream, which in addition to the standard input stream also delinates distinct input events with pipe characters (|)
    public var separatedInputStream : String {
        // separate input events by pipes (|)
        guard self.inputEvents.count > 0 else { return "" }
        
        var ret = ""
        for inputEvents in self.inputEvents {
            ret += String(inputEvents.isDeletion ? "<" : inputEvents.inputString)
            ret += "|"
        }
        
        guard ret.length > 0 else { return "" }
        guard ret.length > 1 else { return self.inputEvents.first!.inputString }
        
        return ret.substring(to: ret.index(before: ret.endIndex))
    }
    
    /// Appends a given input stream fragment to the input stream. Internally, this means translating the given input stream fragment into keystrokes. Note: Composite input events cannot be entered this way (use manual insertion via the inputEvents array instead).
    ///
    /// - Parameter stream: The input stream fragment to append.
    public func appendInputStream(_ stream:String) {
        // for convenience: parse the given input stream and add as Keystrokes.
        for c in stream.characters {
            if c == "<" {
                // backspace
                keystroke(char: Character.backspaceChar)
            }
            else {
                keystroke(char: c)
            }
        }
    }
    
    /// Inits the phrase with another phrase as a template.
    ///
    /// - Parameter withTemplate: The original phrase.
    public convenience init(withTemplate:Phrase) {
        self.init(withTemplate.presented)
    }
    public init(presented:String) {
        
        self.presented = presented
        self.state = .ready
        metrics = PhraseMetrics(phrase: self)
    }
    public convenience init(_ presentedPhrase:String) {
        self.init(presented:presentedPhrase)
    }
    
    public func start() {
        self.startTime = Date()
        self.state = .inProgress
    }
    /// A convenience function that can be used to statelessly record the phrase, e.g. for unit testing.
    ///
    /// - Parameters:
    ///   - transcribed: The transcribed string (as transcribed by the user).
    ///   - inputStream: The associated input stream.
    ///   - time: The amount of time needed by the user.
    ///-Remark: Does not verify that the input stream matches the transcribed string!
    public func recordStateless(transcribed:String, inputStream:String, time:TimeInterval) {
        
        self.state = .ready
        
        self.start()
        self.appendInputStream(inputStream)
        self.finish(withTranscribed: transcribed, time: time)
        
        // self.state == .finished
    }
    
    /// Marks the phrase as finished with the given transcribed string.
    ///
    /// - Parameters:
    ///   - withTranscribed: The string as transcribed by the user.
    ///   - time: The amount of time it took the user. Can be nil (then the measured time will be used.)
    public func finish(withTranscribed:String, time:TimeInterval? = nil ) {
        guard self.state == .inProgress else { return }
        self.transcribed = withTranscribed
        
        if let time = time { self.startTime = nil; self.endTime = nil; self._time = time }
        else { self.endTime = Date() }
        
        self.state = .finished
    }
    
    /// A convenience method that can be used to add a single keystroke (but not a composite input event.)
    ///
    /// - Parameter char: The character to add to the input event.
    public func keystroke(char: Character ) {
        // convenience method. add one keystroke.
        inputEvent(string: String(char))
    }
    
    /// A convenience method that can be used to add one or more characters to the input stream.
    ///
    /// - Parameter string: The string produced by the input event (can be one character or longer).
    public func inputEvent(string: String) {
        
        guard self.state == .inProgress else { return }
        let inputEvent = InputEvent(string:string, time:((self.startTime?.timeIntervalSinceReferenceDate)! - Date().timeIntervalSinceReferenceDate))
        self.inputEvents.append(inputEvent)
        
    }
    
    /// A convenience method that can be used to add a deletion input event.
    /// - Parameter length: The number of characters removed in the deletion event.
    public func deletionEvent(length: Int) {
        guard self.state == .inProgress else { return }
        let inputEvent = InputEvent(deletionLength: length, time:((self.startTime?.timeIntervalSinceReferenceDate)! - Date().timeIntervalSinceReferenceDate))
        self.inputEvents.append(inputEvent)
    }
    
    /// Make this phrase lowercase.
    public func makeLowercase() {
        guard self.state == .ready else { return } // Don't allow changing the phrase after the fact.
        self.presented = presented.lowercased()
    }
    
    /// Make this phrase alphanumeric only (by removing several disallowed characters).
    public func makeAlphanumericOnly() {
        guard self.state == .ready else { return } // Don't allow changing the phrase after the fact.
        // TODO making a string alphanumeric only is actually nontrivial! What about non-Latin scripts? What about umlauts etc?
        // For now, just filter out common special characters.
        let disallowedCharacters : String = ";:,.-´`?'\"/\\<>^!¡@()[]{}"
        
        self.presented = presented.removeCharacters(disallowedCharacters)
    }
    
    /// The correction sequences.
    public var correctionSequences : [String]? {
        guard inputStream != nil, inputStream.length > 0 else { return nil }
        var corrections = [String]()
        
        
        var i : Int = 0, backspacesSeenSoFar : Int = 0, lastCorrectionEndIndex : Int? = nil
        for char in inputStream.characters {
        
            
            if char == "<" {
                //correction
                backspacesSeenSoFar += 1
            }
            else {
                if backspacesSeenSoFar > 0 {
                    let sequenceRange  = max(i-backspacesSeenSoFar*2, 0)...i
                    let startIndex = inputStream.index(inputStream.startIndex, offsetBy:sequenceRange.lowerBound)
                    let endIndex = inputStream.index(inputStream.startIndex, offsetBy:sequenceRange.upperBound )
                    let stringRange = startIndex..<endIndex
                    var correctionSequence = inputStream.substring(with: stringRange)
                    
                    if lastCorrectionEndIndex != nil, lastCorrectionEndIndex == sequenceRange.lowerBound {
                            // we have a previous sequence that is contiguous with this one. merge the two to one correction sequence
                        correctionSequence = corrections.last! + correctionSequence
                        corrections[corrections.count - 1] = correctionSequence
                    }
                    else {
                        
                        corrections.append(correctionSequence)
                        
                    }
                    
                    lastCorrectionEndIndex = i
                    
                    backspacesSeenSoFar = 0
                }
                
            }
            
            i += 1
        }
        
        // merge corrections
        
        
        return corrections
    }
}
