//
//  Extensions.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 10/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import Foundation
import UIKit
public extension Character {
    
    /// A unicode scalar point representation of this character.
    var unicodeScalarCodePoint : UInt32 {
        let characterString = String(self)
        let scalars = characterString.unicodeScalars
        
        return scalars.first!.value
    }
    
    /// The backspace char (codepoint 8).
    public static var backspaceChar: Character {
        
        return Character(UnicodeScalar(8))
    }
}
public extension String {
    
    /// The string's length.
    var length : Int {
        return self.characters.count
    }

    
    /// Returns the character at index `i`.
    ///
    /// - Parameter i: The requested index.
    subscript (i: Int) -> String {
        return String(self[self.characters.index(self.startIndex, offsetBy: i)])
    }
    
    public static func levenshteinDistance(_ string1: String, _ string2: String) -> Int {
        let (length1, length2) = (string1.characters.count, string2.characters.count)
        
        guard length2 > 0 else {
            return string1.characters.count
        }
        
        var matrix = Array(repeating: Array(repeating: 0,
                                            count: length1 + 1),
                           count: length2 + 1)
        
        for m in 1 ..< length2 {
            matrix[m][0] = matrix[m - 1][0] + 1
        }
        
        for n in 1 ..< length1 {
            matrix[0][n] = matrix[0][n - 1] + 1
        }
        
        for m in 1 ..< (length2 + 1) {
            for n in 1 ..< (length1 + 1) {
                
                let penalty = (string1[n - 1] == string2[m - 1] ? 0 : 1)
                let (horizontal, vertical, diagonal) = (matrix[m - 1][n] + 1, matrix[m][n - 1] + 1, matrix[m - 1][n - 1])
                matrix[m][n] = min(horizontal, vertical, diagonal + penalty)
            }
        }
        
        return matrix[length2][length1]
    }
    
    internal var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle(identifier: "inso.InputMethodEvaluationSuite")!, value: "", comment: "")
    }
    
    /// Returns a string from which certain characters have been removed.
    ///
    /// - Parameter disallowedCharacters: The characters to be removed.
    /// - Returns: A string with the specified characters removed.
    public func removeCharacters(_ disallowedCharacters : String) -> String {
        // return a string without the characters
        return String(self.characters.filter({
            (char : Character) -> Bool in
            var include = true
            for c in disallowedCharacters.characters {
                if char == c { include = false; break }
            }
            return include
        }))
    }
    
    

}
public extension UIDevice {
    // https://gist.github.com/JonFir/008771d8482924ed0941
    /// The model name as a string (from https://gist.github.com/JonFir/008771d8482924ed0941)
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}
/// An extension to facilitate cloning phrasesets.
internal extension Array where Element : Phrase {
    /// Clones the given set of phrases into a new one, possibly with flags.
    ///
    /// - Parameters:
    ///   - lowercase: Whether to make the new phrase set lowercase.
    ///   - alphanumericOnly: Whether to make the new phrase set alphanumeric (by excluding certain special characters).
    /// - Returns: The cloned phrase set, with the given modifications applied.
    func clone(lowercase:Bool = false, alphanumericOnly: Bool = false) -> [Phrase] {
        var clonedArray : [Phrase] = [Phrase]()
        for phrase in self {
            let newPhrase = Phrase(withTemplate:phrase)
            if lowercase { newPhrase.makeLowercase() }
            if alphanumericOnly { newPhrase.makeAlphanumericOnly() }
            clonedArray.append(newPhrase)
        }
        return clonedArray
    }
    
}
