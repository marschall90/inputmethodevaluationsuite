//
//  KeyboardManager.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 24/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//


import UIKit
public class KeyboardManager {
    
    /// Returns a list with the names of all keyboards currently installed on the system.
    /// - Remark: Names are user-readable and not bundle identifiers.
    public static var installedKeyboards : [String] {
        return UITextInputMode.activeInputModes.map({
            (inputMode:UITextInputMode) -> String in
            return inputMode.value(forKey: "displayName") as! String
        })
    }
    
    /// Returns the name of the keyboard that is currently displayed on screen.
    /// - Precondition: The keyboard must be on screen for this method to return the current keyboard's name. Otherwise, `nil` will be returned.
    public static var currentlyDisplayedKeyboard : String? {
        let currentInputMode : [UITextInputMode] = UITextInputMode.activeInputModes.filter({ (inputMode:UITextInputMode) -> Bool in
            if let isDisplayed = inputMode.value(forKey: "isDisplayed"), isDisplayed is Bool {
                return isDisplayed as! Bool
            }
            else { return false }
        })
        guard currentInputMode.count > 0 else { return nil }
        
        return (currentInputMode.first!.value(forKey: "displayName") as! String)
    }
}
