//
//  InputEvent.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 13/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import Foundation

/// An input event. This can be a keystroke or a composite input gesture (which might insert multiple characters at once). If isDeletion is true, this event represents a deletion rather than an insertion.
public class InputEvent {
    public var inputString : String
    public var time : TimeInterval
    
    /// Whether this was a deletion event.
    public var isDeletion : Bool {
        for char in inputString.characters {
            // check if all characters are backspaces
            
            if (char.unicodeScalarCodePoint != 8) { // 8 is backspace
                return false
            }
        }
        
        return true
    }
    
    /// init
    ///
    /// - Parameters:
    ///   - char: The character that represents this InputEvent.
    ///   - time: The time at which this InputEvent occurred.
    public init(string: String, time: TimeInterval) {
        self.inputString = string
        self.time = time
    }
    public init(deletionLength: Int, time: TimeInterval) {
        var deletionSeq = ""
        for _ in 0..<deletionLength {
            deletionSeq += String(Character.backspaceChar)
        }
        self.inputString = deletionSeq
        self.time = time
    }
}
