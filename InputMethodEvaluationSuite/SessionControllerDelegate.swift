//
//  SessionControllerDelegate.swift
//  InputMethodEvaluationSuite
//
//  Copyright © 2017 Marcel Hansemann. All rights reserved.


import UIKit

/// Implement this protocol to be notified by the session controller about state changes. 
public protocol SessionControllerDelegate {
    func didShowLastPhraseInRound(round: Round)
    func showingPhrase(atIndex:Int, round:Round)
    func shouldShowPhrase(atIndex:Int, round:Round) -> Bool // return false to skip this phrase, or just to other stuff to prepare for this phrase
}
public extension SessionControllerDelegate {
    func didShowLastPhraseInRound(round: Round) {
    }
    func showingPhrase(atIndex:Int, round:Round) {
    
    }
    func shouldShowPhrase(atIndex:Int, round:Round) -> Bool {
        return true
    }
}
