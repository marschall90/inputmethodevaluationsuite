//
//  Round.swift
//  InputMethodEvaluationSuite
//
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import Foundation
/// One evaluation round, which has a particular configuration. For example, one round might represent one trial run with a particular input method.
public class Round : CustomStringConvertible, KeyValueDescribable, KeyValueArrayInitializable {
   
    public var configuration : RoundConfiguration
    public lazy var phrases = [Phrase]()
    
    public lazy var metrics : RoundMetrics = RoundMetrics(round: self)
    public var phraseIndex : Int = 0
    public var currentPhrase : Phrase? {
        guard phraseIndex < phrases.count else { return nil }
        return phrases[phraseIndex]
    }
    
    public enum RoundState {
        case ready, inProgress, cancelled, finished
    }
    public var state : RoundState
    
    public var startTime : Date?
    public var endTime : Date?
    public var duration : TimeInterval {
        guard startTime != nil, endTime != nil else {
            return 0
        }
        return endTime!.timeIntervalSinceReferenceDate - startTime!.timeIntervalSinceReferenceDate
        
    }
    public static var supportedKeys: [MetricKey] {
        return [.roundMetrics, .roundConfiguration, .phrases]
    }
    public var keyValueDescription: [(key: MetricKey, value: Any)] {
        return [(key:.roundMetrics, value: metrics.precomputedKeyValueDescription), (key:.roundConfiguration, value: configuration.keyValueDescription), (key:.phrases, value: phrases.keyValueDescription)]
    }
    public var description: String {
        return description(usePrecomputedValues: false)
    }
    public func description(usePrecomputedValues:Bool) -> String{
        var str = "Round ------ {\nDuration: \(self.duration)\nConfiguration:\n\(self.configuration.description)\n\nMetrics:\n{\(self.metrics.description(usePrecomputedValues:usePrecomputedValues)) \n}\n\nPhrases:\n"
        for phrase in phrases {
            str += phrase.description(usePrecomputedValues: usePrecomputedValues)
        }
        str += "\n}"
        return str
    }
    // Convenience initializer: Allows constructing a round with strings rather than Phrases.
    public convenience init(withConfiguration:RoundConfiguration, strings:[String])  {
        // construct an array of Phrases from the supplied Strings.
        let phrases = strings.map({return Phrase($0)})
        self.init(withConfiguration:withConfiguration, phraseList:phrases)
    }
    
    public required convenience init(withKeyValueDescriptions: [(key: MetricKey, value: Any)]) {
        var metrics : RoundMetrics?
        var config : RoundConfiguration?
        var phrases : [Phrase]?
        
        for (key, value) in withKeyValueDescriptions {
            switch key {
            case .roundMetrics:
                if let metricsKVD = value as? [(key:MetricKey, value:Any)]/*, let metrics = RoundMetrics(withKeyValueDescriptions: metricsKVD)*/ {
                    metrics = RoundMetrics(withKeyValueDescriptions: metricsKVD)
                    
                }
            case .roundConfiguration:
                if let rcKVD = value as? [(key:MetricKey, value:Any)] {
                    config = RoundConfiguration(withKeyValueDescriptions: rcKVD)
                }
            case .phrases:
                if let phrasesKVD = value as? [[(key:MetricKey, value:Any)]] {
                    phrases = [Phrase]()
                    for phraseKVD in phrasesKVD {
                        phrases!.append(Phrase(withKeyValueDescriptions:phraseKVD))
                    }
                }
            default:
                break
            }
        }
        
        
        if let config = config, let phrases = phrases, let metrics = metrics {
            self.init(withConfiguration: config, phraseList: phrases)
            self.metrics = metrics
        }
        else {
            // TODO should nil here and make init optional!
            
            self.init(withConfiguration: RoundConfiguration(),
                      phraseList: [Phrase("No phrases loaded")])
        }
    }
    
    public func applyConfigurationChanges() {
        // Call this method to reload the phraseset from the associated round configuration, given that there are changes.
        // this is only possible if the round's state is "ready", so that data cannot be changed "after the fact".
        guard self.state == .ready else { return }
        preparePhrases(phraseList: nil)
        
    }
    private func preparePhrases(phraseList : [Phrase]?) {
        // MUST have assigned round configuration before!
        
        var usedPhraseList : [Phrase]
        
        if phraseList == nil {
            // no extra phraselist was supplied -- read in the phraseset specified in the configuration instead.
            let loadedPhraseset = PhrasesetManager.sharedInstance.phrases(forPhraseset: configuration.phraseSet, lowercase: configuration.lowercaseOnly, alphanumericOnly: configuration.alphanumericOnly)
            usedPhraseList = loadedPhraseset
        }
        else { usedPhraseList = phraseList!.clone(lowercase: configuration.lowercaseOnly, alphanumericOnly: configuration.alphanumericOnly) }
        
        var orderedList : [Phrase]
        switch configuration.phrasePresentationOrder {
        case .originalOrder:
            orderedList = usedPhraseList
        case .alphabetic:
            orderedList = usedPhraseList.sorted(by: {
                (phraseA : Phrase, phraseB : Phrase) -> Bool in
                return phraseA.presented < phraseB.presented
            })
        case .random:
            orderedList = usedPhraseList.sorted(by: {
                (phraseA : Phrase, phraseB : Phrase) -> Bool in
                let randomNum:Int = Int(arc4random_uniform(1))
                return randomNum < 1
            })
            
        }
        
        
        // restrict number of phrases to specified phrase count (if specified value is > 0)
        if configuration.phraseCount > 0, configuration.phraseCount < orderedList.count {
            orderedList.removeLast(orderedList.count - configuration.phraseCount)
        }
        
        self.phrases.append(contentsOf: orderedList)
    }
    public init(withConfiguration:RoundConfiguration, phraseList:[Phrase]?) {
        self.configuration = withConfiguration
        self.state = .ready
        preparePhrases(phraseList: phraseList)
    }
    public convenience init(withConfiguration:RoundConfiguration) {
        self.init(withConfiguration: withConfiguration, phraseList: nil)
    }
    public var hasNextPhrase : Bool {
        return phraseIndex < phrases.count - 1
    }
    public func start() {
        // loadPhrases()
        self.startTime = Date()
        self.state = .inProgress
        
    }
    public func nextPhrase() -> Bool {
        // advances to the next phrase, if possible, and returns true; or returns false if not possible
        
        //guard self.state == .inProgress else { return false }
        if !hasNextPhrase {
            return false
        }
        else {
            
            phraseIndex += 1
        }
        return true
    }
    public func finish() {
        guard self.state == .inProgress else { return }
        
        self.endTime = Date()
        if self.hasNextPhrase {
            self.state = .cancelled
        }
        else {
            self.state = .finished
        }
    }
}
