//
//  MetricKey.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 15/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import Foundation

/// An enum with keys for various phrase and round metrics.
public enum MetricKey : String {
     case wordsPerMinute, backspaceCount, keystrokesPerCharacter, correctKeystrokes,  incorrectNotFixed, fixes, incorrectFixed,  correctedErrorRate, uncorrectedErrorRate, totalErrorRate, correctionEfficiency,  participantConscientiousness, utilizedBandwidth, wastedBandwidth, presentedCharCount, transcribedCharCount, inputTime, minimumStringDistance, costPerCorrection, msdErrorRate // Metrics
    case sessionName, deviceName, rounds // Session
    case roundMetrics, roundConfiguration, phrases // Round
    case presented, transcribed, inputStream, duration // Phrase
    case autocorrectEnabled, allowsMulticharacterInsertion, lowercaseOnly, keyboardName, phraseCount, autocapitalizationEnabled, bindSpacesToPreviousMulticharacterEvent, phrasePresentationOrder, phraseset, alphanumericOnly // Round configuration

    
}
