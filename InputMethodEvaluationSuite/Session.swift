//
//  Session.swift
//  InputMethodEvaluationSuite
//
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import Foundation
import UIKit

public let descriptionKey = "description"

/// An evaluation session - a composite of multiple rounds.
public class Session :  NSObject, KeyValueDescribable, NSCoding {
    
    public func encode(with aCoder: NSCoder) {
        // TODO this only saves/retrieves the flat session description
        aCoder.encode(self.description, forKey:descriptionKey)
        aCoder.encode(self.sessionName, forKey:MetricKey.sessionName.rawValue)
        aCoder.encode(self.deviceName, forKey:MetricKey.deviceName.rawValue)
    }
 
    public required init?(coder aDecoder: NSCoder) {
        // TODO this only saves/retrieves the flat session description
        if let sessionName = aDecoder.decodeObject(forKey:MetricKey.sessionName.rawValue) as? String, let description =  aDecoder.decodeObject(forKey:descriptionKey) as? String, let deviceName = aDecoder.decodeObject(forKey: MetricKey.deviceName.rawValue) as? String {
            self.sessionName = sessionName
            _description = description
            self.deviceName = deviceName
            self.state = .finished
        }
        else {
            // no session name! we cannot construct the session object
            return nil
        }
    }
    
    private var _description : String? = nil
    
    /// A human-readable string describing the session parameters and content (Rounds and their constituent parts).
    public override var description: String {
        if _description != nil { return _description! } // return a stored description if we have one
        return description(usePrecomputedValues:false)
    }
    public func description(usePrecomputedValues:Bool) -> String {
        if _description != nil { return _description! }
        
        var str = "Session '\(sessionName)' {\nDevice: \(deviceName)\nRounds:\n\n"
        for round in rounds {
            str += round.description(usePrecomputedValues:usePrecomputedValues)
        }
        str += "\n}"
        return str
    }
    public static var supportedKeys: [MetricKey] {
        return [.sessionName, .deviceName]
    }
    public var keyValueDescription: [(key: MetricKey, value: Any)] {
        return [(key:.sessionName, self.sessionName), (key:.deviceName, self.deviceName), (key:.rounds, self.rounds.keyValueDescription)]
    }
    
    
    /// A name to uniquely identify the session.
    public var sessionName : String
    /// The name of the current device (e.g. iPhone 6).
    public var deviceName : String
    
    
    /// The evaluation rounds this session consists of (each evaluation round can have its own phraseset and keyboard).
    public lazy var rounds : [Round]  = [Round]()
    
    /// The state of a session. Will be changed either manually or by the session controller.
    ///
    /// - ready: Default. The session hasn't started yet.
    /// - inProgress: The session is ongoing.
    /// - cancelled: The session is cancelled (not all rounds have been completed).
    /// - finished: The session is finished / archived. All rounds have been conducted.
    public enum SessionState {
        case ready, inProgress, cancelled, finished
    }
    
    /// The session state (see *SessionState*)
    public private(set) var state : SessionState
    
    /// The current round in progress (read-only).
    public var currentRound : Round? {
        guard roundIndex < rounds.count else { return nil }
        return rounds[roundIndex]
    }
    
    /// The current round index.
    public private(set) var roundIndex : Int = 0
    
    
    /// Returns whether all rounds have either been completed or cancelled.
    public var areAllRoundsFinished : Bool {
        for round in self.rounds {
            if !(round.state == .finished || round.state == .cancelled) { return false }
        }
        return true
    }
    
    /// init
    ///
    /// - Parameter sessionName: A name to identify this session.
    public init(sessionName: String) {
        self.sessionName = sessionName
        self.deviceName = UIDevice.current.modelName
        self.state = .ready
    }
    
    /// Start this session. State changes to `.inProgress`.
    ///- Precondition: State must be == `.ready`. Otherwise no effect.
    public func start() {
        
        guard self.state == .ready else { return }
        guard self.rounds.count > 0 else {
            print("No rounds.")
            return
        }
        
        self.state = .inProgress
    }
    
    /// Specifies whether the session has a next / upcoming round. If this is false, the current round is the final one in the session.
    public var hasNextRound : Bool {
        return roundIndex < rounds.count - 1
    }
    
    /// Advances to the next round.
    ///
    /// - Returns: A value indication whether advancing was possible. If `false` is returned, that means the current session was the final one.
    public func nextRound() -> Bool {
        // advances to the next round, if possible, and returns true; or returns false if not possible
        guard self.state == .inProgress else { return false }
        if !hasNextRound {
            return false
        }
        else {
            roundIndex += 1
        }
        return true
    }
    
    /// Stop / finish this session. State changes to `.finished`.
    ///- Precondition: State must be == `.inProgress`. Otherwise no effect.
    public func finish() {
        guard self.state == .inProgress else { return }
        
        if self.hasNextRound {
            self.state = .cancelled
        }
        else {
            self.state = .finished
        }
    }
    
    /// Archives the session (renders it stateless). Call this function to make sure that everything in this session (Phrases, Rounds) has state `.finished` or `.cancelled` (it's tinned).
    ///- Remark: Note that the current implementation might falsify time data on phrases that haven't actually been presented to the user.
    public func archive() {
        
        for round in rounds {
            for phrase in round.phrases {
                if !(phrase.state == .finished) { phrase.state = .finished }
            }
            if !(round.state == .finished || round.state == .cancelled) { round.state = .cancelled }
        }
    }
}
