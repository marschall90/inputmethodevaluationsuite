//
//  RoundConfiguration.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 10/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import Foundation
/// A class that describes the settings for one particular round (e.g. which keyboard to use, which phraseset etc.)
public class RoundConfiguration : CustomStringConvertible, KeyValueDescribable, KeyValueArrayInitializable {
    public var phraseSet : String = ""
    public var alphanumericOnly : Bool = false
    
    
    /// Whether the keyboard's autocorrection feature is enabled (this usually only applies to the original iOS keyboard).
    ///- Remark: When enabling this setting, multicharacter insertion should also be enabled. Otherwise, the autocorrection feature will not work properly.
    public var autocorrectEnabled : Bool = false
    
    /// Whether to allow the insertion of more than a single character in a given input event. This is a necessary setting for e.g. gesture-based input methods, which might submit just one input event per word (or even phrase).
    public var allowsMulticharacterInsertion : Bool = false
    public var isTrainingRound : Bool = false
    public var lowercaseOnly : Bool = false
    
    /// A human-readable string indicating the user-facing name of the keyboard used in this session.
    public var keyboardName : String = ""
    
    /// The number of phrases to present to the user. If `phraseCount` < the size of the phraseset, this will result in restricting the number of phrases shown. Otherwise, the full phraseset will be shown.
    ///- Remark: Setting `phraseCount` to 0 will also show all phrases in the set.
    public var phraseCount : Int = 10 // 0 means "present all phrases in the set / don't restrict the number of phrases presented"
    public var autocapitalizationEnabled : Bool = false
    
    
    /// Whether to include spaces (" ") in the previous input event, if that input event consisted of more than one character. 
    ///- Remark: This setting is appropriate for gesture-based keyboards. These often submit words and subsequent spaces as separate input events, even though the user made no additional input gesture to trigger the insertion of a space character. Enabling `bindSpacesToPreviousMulticharacterEvent` will merge the space insertion into the previous composite input event.
    public var bindSpacesToPreviousMulticharacterEvent : Bool = true
    
    
    
    public enum PhrasePresentationOrder : String {
        case originalOrder = "Original order", random = "Random", alphabetic = "Alphabetic"
    }
    public var phrasePresentationOrder : PhrasePresentationOrder = .random
    
    public var description: String {
        return "Configuration { Phrase set: \(phraseSet), Keyboard name: \(keyboardName), Phrase count: \(phraseCount), Phrase presentation order: \(phrasePresentationOrder.rawValue), Autocorrect: \(autocorrectEnabled ? "yes" : "no"), Autocapitalize: \(autocapitalizationEnabled ? "yes" : "no"), Lowercase only: \(lowercaseOnly ? "yes" : "no"), Alphanumeric only: \(alphanumericOnly ? "yes" : "no") }"
    }
    
    public static var supportedKeys: [MetricKey] {
        return [.autocorrectEnabled, .allowsMulticharacterInsertion, .lowercaseOnly, .keyboardName, .phraseCount, .autocapitalizationEnabled, .bindSpacesToPreviousMulticharacterEvent, .phrasePresentationOrder, .phraseset]
    }
    public var keyValueDescription: [(key: MetricKey, value: Any)] {
        return [(key:.autocorrectEnabled, value:autocorrectEnabled),
                (key:.allowsMulticharacterInsertion, value:allowsMulticharacterInsertion),
                (key:.lowercaseOnly,value:lowercaseOnly),
                (key:.keyboardName,value:keyboardName),
                (key:.phraseCount,value:phraseCount),
                (key:.autocapitalizationEnabled,value:autocapitalizationEnabled),
                (key:.bindSpacesToPreviousMulticharacterEvent, value:bindSpacesToPreviousMulticharacterEvent),
                (key:.phrasePresentationOrder,value:phrasePresentationOrder),
                (key:.phraseset, value: phraseSet)]
    }
    
    
    public init() {
        // nothing
    }
    public required convenience init(withKeyValueDescriptions: [(key: MetricKey, value: Any)]) {
        self.init()
        
        let kvds = withKeyValueDescriptions
        for kvd in kvds {
            switch kvd.key {
            case .allowsMulticharacterInsertion:
                allowsMulticharacterInsertion = kvd.value as? Bool ?? false
            case .alphanumericOnly:
                alphanumericOnly = kvd.value as? Bool ?? false
            case .autocorrectEnabled:
                autocorrectEnabled = kvd.value as? Bool ?? false
            case .lowercaseOnly:
                lowercaseOnly = kvd.value as? Bool ?? false
            case .keyboardName:
                keyboardName = kvd.value as? String ?? ""
            case .phraseCount:
                phraseCount = kvd.value as? Int ?? 0
            case .autocapitalizationEnabled:
                autocapitalizationEnabled = kvd.value as? Bool ?? false
            case .bindSpacesToPreviousMulticharacterEvent:
                bindSpacesToPreviousMulticharacterEvent = kvd.value as? Bool ?? false
            case .phrasePresentationOrder:
                if !(kvd.value is String) { phrasePresentationOrder = .originalOrder }
                else { phrasePresentationOrder = PhrasePresentationOrder(rawValue:(kvd.value as! String)) ?? .originalOrder }
            case .phraseset:
                phraseSet = kvd.value as? String ?? ""
                
            default:
                break;
            }
        }
    }
    /*init(keyboardName:String,
        phraseSetName:String,
        autocorrectEnabled : Bool = false,
        alphanumericOnly : Bool = false,
        lowercaseOnly : Bool = false,
        isTrainingRound : Bool = false) {
        self.phraseSetName = phraseSetName
        self.alphanumericOnly = alphanumericOnly
        self.autocorrectEnabled = autocorrectEnabled
        self.isTrainingRound = isTrainingRound
        self.lowercaseOnly = lowercaseOnly
        self.keyboardName = keyboardName
    }*/
}
