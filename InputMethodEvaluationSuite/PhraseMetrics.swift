//
//  PhraseMetrics.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 12/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import Foundation
public class PhraseMetrics : KeyValueDescribable, CustomStringConvertible {
    
    
    public var phrase : Phrase
    public init(phrase: Phrase) {
        self.phrase = phrase
        
    }
    
    /// (`KeyValueDescribable`) The metric keys that can be returned by `keyValueDescription`.
    public static var supportedKeys : [MetricKey] {
        return [.wordsPerMinute, .backspaceCount, .keystrokesPerCharacter, .correctKeystrokes, .incorrectNotFixed, .fixes, .incorrectFixed, .correctedErrorRate, .uncorrectedErrorRate, .totalErrorRate, .correctionEfficiency, .participantConscientiousness, .utilizedBandwidth, .wastedBandwidth, .minimumStringDistance, .costPerCorrection, .msdErrorRate]
    }
    
    /// (`KeyValueDescribable`) Returns a description of the phrase metrics in the form of an array with `(key:, value:)` tuples.
    public var keyValueDescription: [(key: MetricKey, value: Any)] {
        _precomputedKeyValueDescription = [(key: .wordsPerMinute, value:wordsPerMinute),
                (key: .minimumStringDistance, value:minimumStringDistance),
                (key: .backspaceCount, value:backspaceCount),
                (key: .keystrokesPerCharacter, value:keystrokesPerCharacter),
                (key: .correctKeystrokes, value:correctKeystrokes),
                (key: .incorrectNotFixed, value:incorrectNotFixed),
                (key: .fixes, value:fixes),
                (key: .incorrectFixed, value:incorrectFixed),
                (key: .correctedErrorRate, value:correctedErrorRate),
                (key: .uncorrectedErrorRate, value:uncorrectedErrorRate),
                (key: .totalErrorRate, value:totalErrorRate),
                (key: .correctionEfficiency, value:correctionEfficiency),
                (key: .participantConscientiousness, value:participantConscientiousness),
                (key: .utilizedBandwidth, value:utilizedBandwidth),
                (key: .wastedBandwidth, value:wastedBandwidth),
                (key: .costPerCorrection, value:costPerCorrection),
                (key: .msdErrorRate, value:msdErrorRate)
            
        ]
        return _precomputedKeyValueDescription!
        
        
    }
    
    
    public var _precomputedKeyValueDescription : [(key: MetricKey, value: Any)]? = nil
    public var precomputedKeyValueDescription : [(key: MetricKey, value: Any)] {
        if (_precomputedKeyValueDescription == nil) {
            _precomputedKeyValueDescription = keyValueDescription
        }
        return _precomputedKeyValueDescription!
    }
    
    /// A human-readable description of the phrase metrics.
    public var description: String {
        return description(usePrecomputedValues: false)
    }
    public func description(usePrecomputedValues:Bool) -> String {
        let description = usePrecomputedValues ? self.precomputedKeyValueDescription : self.keyValueDescription
        return description.map({
            (key:MetricKey, value:Any) -> String in
            return "\(key.rawValue.localized): \(value is Double ? String(format: "%.3f", value as! Double) : String(describing: value))"
        }).joined(separator: "\n")
    }
    /// Words per minute.
    /// - Precondition: Phrase must already be transcribed.
    public var wordsPerMinute : Double {
        guard phrase.isTranscribed else { return 0 }
        
        return max(((Double(phrase.transcribed!.length - 1) / phrase.duration) * 60) / 5, 0)
    }
    
    /// The amount of backspaces in the input stream.
    /// - Precondition: Phrase must already be transcribed.
    public var backspaceCount : Int {
        guard phrase.isTranscribed else { return 0 }
        
        return phrase.inputEvents.filter{$0.isDeletion}.count
    }
    
    
    /// Keystrokes per character.
    public var keystrokesPerCharacter : Double {
        guard phrase.isTranscribed, phrase.inputStream.length > 0, phrase.transcribed!.length > 0 else {
            return 0
        }
        
        return Double(phrase.inputStream.length) / Double(phrase.transcribed!.length)
    }
    
    
    /// The minimum string distance between the presented and transcribed strings.
    public var minimumStringDistance : Int {
        guard phrase.isTranscribed else { return 0 }
        
        return String.levenshteinDistance(phrase.presented, phrase.transcribed!)
    }
    
    /// The MSD error rate.
    public var msdErrorRate : Double {
        guard phrase.isTranscribed, phrase.transcribed!.length > 0 else {
            return 0
        }
        
        return Double(minimumStringDistance) / Double(max(phrase.presented.length, phrase.transcribed!.length))
    }
    
    /// The amount of correct keystrokes.
    public var correctKeystrokes : Int {
        guard phrase.isTranscribed else { return 0 }
        return max(phrase.presented.length, phrase.transcribed!.length) - String.levenshteinDistance(phrase.presented, phrase.transcribed!)
    }
    
    /// The amount of incorrect and unfixed keystrokes.
    public var incorrectNotFixed : Int {
        guard phrase.isTranscribed else { return 0 }
        
        return minimumStringDistance
    }
    
    /// The amount of fixes (backspaces).
    public var fixes : Int {
        guard phrase.isTranscribed else { return 0 }
        
        return self.backspaceCount
    }
    
    
    /// The amount of fixed incorrect keystrokes.
    public var incorrectFixed : Int {
        guard phrase.isTranscribed else { return 0 }
        // TODO needs to be: all characters backspaced during entry. so characters not in the transcribed text. So we need to check which are there !! (dictionary?)
        
        return self.backspaceCount
    }
    

    /// The corrected error rate.
    public var correctedErrorRate : Double {
        guard phrase.isTranscribed else { return 0 }
        
        return (Double(incorrectFixed) / Double(correctKeystrokes + incorrectNotFixed + incorrectFixed)) 
    }
    
    /// The uncorrected error rate.
    public var uncorrectedErrorRate : Double {
        guard phrase.isTranscribed else { return 0 }
        
        return (Double(incorrectNotFixed) / Double(correctKeystrokes + incorrectNotFixed + incorrectFixed))
    }
    
    /// The total error rate (=corrected + uncorrected).
    public var totalErrorRate : Double {
        guard phrase.isTranscribed else { return 0 }
        
        return correctedErrorRate + uncorrectedErrorRate
    }
    public var correctionEfficiency : Double {
        guard phrase.isTranscribed, fixes > 0 else { return 0 }
        return Double(incorrectFixed) / Double(fixes)
    }
    public var participantConscientiousness : Double {
        guard phrase.isTranscribed, (incorrectFixed + incorrectNotFixed) > 0 else { return 0 }
        return Double(incorrectFixed) / Double(incorrectFixed + incorrectNotFixed)
    }
    public var utilizedBandwidth : Double {
        guard phrase.isTranscribed, (correctKeystrokes + incorrectNotFixed + incorrectFixed + fixes) > 0 else { return 0 }
        return Double(correctKeystrokes) / Double(correctKeystrokes + incorrectNotFixed + incorrectFixed + fixes)
    }
    public var wastedBandwidth : Double {
        guard phrase.isTranscribed, (correctKeystrokes + incorrectNotFixed + incorrectFixed + fixes) > 0 else { return 0 }
        return Double(incorrectNotFixed + incorrectFixed + fixes) / Double(correctKeystrokes + incorrectNotFixed + incorrectFixed + fixes)
    }
    
    /// The cost per correction made. This utilizes `Phrase.correctionSequence` decomposition.
    public var costPerCorrection : Double {
        guard phrase.isTranscribed, phrase.inputStream != "", phrase.correctionSequences != nil, phrase.correctionSequences!.count > 0 else { return 0 }
        
        return Double(incorrectFixed + fixes) / Double(phrase.correctionSequences!.count)
    }
}
