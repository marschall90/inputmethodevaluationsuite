//
//  TestObj.swift
//  InputMethodEvaluationSuite
//
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

import UIKit
/// The session controller coordinates session handling. 
public class SessionController : NSObject, UITextViewDelegate {
    public static let sharedInstance = SessionController()
    
    // contains round results, for use by the table view data source. (This is to prevent lags in the table view)
    private var _roundResultCaches: [[(key:MetricKey, value: Any)]]?
    public var roundResultCaches : [[(key:MetricKey, value: Any)]]?   {
        guard self.session != nil else { return nil  }
        if _roundResultCaches == nil {
            
            var roundResults = [[(key:MetricKey, value: Any)]]()
            for round in self.session!.rounds {
                roundResults.append(round.metrics.keyValueDescription)
                
            }
            _roundResultCaches = roundResults
        }
        
        return _roundResultCaches
    }
    
    
    private var _session : Session?
    public var session : Session? {
        set(value) {
            _session = value
            _roundResultCaches = nil
        }
        get {
            return _session
        }
    }
    
    public var delegate : SessionControllerDelegate?
    
    private var _presentedTextView : UITextView?
    public var presentedTextView : UITextView? {
        set(value) {
            _presentedTextView = value
            guard value != nil else { return }
            value!.text = self.session?.currentRound?.currentPhrase?.presented ?? ""
        }
        get {
            return _presentedTextView
        }
    }
    private var _transcribedTextView : UITextView?
    public var transcribedTextView : UITextView? {
        set(value) {
            _transcribedTextView?.delegate = nil
            _transcribedTextView = value
            guard value != nil else { return }
            value!.text = ""
            value!.delegate = self
        }
        get {
            return _transcribedTextView
        }
    }
    
    public func showNextPhrase() {
        guard session?.currentRound?.currentPhrase != nil, presentedTextView != nil, transcribedTextView != nil else { return }
        
        let previousPhrase = session!.currentRound!.currentPhrase!
        
        // save the transcribed text
        previousPhrase.finish(withTranscribed: transcribedTextView!.text)
        
        
        guard session!.currentRound!.nextPhrase() == true else {
            // we've arrived at the final phrase
            delegate?.didShowLastPhraseInRound(round: session!.currentRound!)
            return
        }
        
        
        conformToRoundConfiguration() // set settings here from the configuration, e.g. autocorrect etc.
        
        let newPhrase = session!.currentRound!.currentPhrase!
        if (session!.currentRound!.configuration.alphanumericOnly) {
            newPhrase.makeAlphanumericOnly()
        }
        if (session!.currentRound!.configuration.lowercaseOnly) {
            newPhrase.makeLowercase()
            transcribedTextView!.autocapitalizationType = .none // override the autocapitalization parameter if this is a lowercase phrase (doesn't make sense to autocapitalize if it's a lowercase phrase)
        }
        
        presentedTextView!.text = newPhrase.presented
        transcribedTextView!.text = ""
        transcribedTextView!.becomeFirstResponder()
        session!.currentRound!.currentPhrase!.start()
        
        // call delegate method to notify any listeners about the new phrase
        delegate?.showingPhrase(atIndex:session!.currentRound!.phraseIndex, round:session!.currentRound!)
    }
    
    // applies the current round configuration to the given text view
    public func applyRoundConfigurationToTextView(_ textView : UITextView) {
        
        if let roundConfiguration = session?.currentRound?.configuration {
            textView.autocapitalizationType = roundConfiguration.autocapitalizationEnabled ? .sentences : .none
            textView.autocorrectionType = roundConfiguration.autocorrectEnabled ? .yes : .no
            
        }
    }
    private func conformToRoundConfiguration() {
        // make sure the textfields' behaviour corresponds to the current round's configuration (e.g. regarding autocorrect).
        guard transcribedTextView != nil else { return }
        applyRoundConfigurationToTextView(transcribedTextView!)
        
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard transcribedTextView != nil, textView === transcribedTextView else { return true }
        
        // for every key press
        if text.length == 0 {
            // the user has deleted one or more characters. Count as deletion event.
             if let phrase : Phrase = session?.currentRound?.currentPhrase, phrase.state == .inProgress {
                phrase.deletionEvent(length: range.length)
            }
            
            return true
        }
        else if text == "\n" {
            // user has pressed the return key -- do not insert, but rather use this as a cue to show to the next phrase
            showNextPhrase()
            return false
        }
        else if text.length == 1 {
            // a single keystroke
            if let phrase : Phrase = session?.currentRound?.currentPhrase, phrase.state == .inProgress {
                phrase.keystroke(char: text.characters.first!)
                return true
            }
        }
        else {
            // inserted more than one character.
            // TODO: should probably prevent this, or not? Is currently unhandled.
            // still count as one "Keystroke"? Block this!!
            // Update: This is also called when inserting Autocorrect suggestions! So check if we are allowing autocorrect, allow the substitution, and replace the text.
            
            if let round = session?.currentRound, round.configuration.allowsMulticharacterInsertion == true, let phrase = round.currentPhrase {
                // count as one deletion and insertion.
                if range.length > 0 {
                    phrase.deletionEvent(length: range.length)
                }
                if text.length > 0 {
                    phrase.inputEvent(string: text)
                }
                return true
            }
            else { return false }
            
        }
        return true
    }
    
}
