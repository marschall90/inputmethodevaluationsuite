//
//  InputMethodEvaluationSuite.h
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 19/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for InputMethodEvaluationSuite.
FOUNDATION_EXPORT double InputMethodEvaluationSuiteVersionNumber;

//! Project version string for InputMethodEvaluationSuite.
FOUNDATION_EXPORT const unsigned char InputMethodEvaluationSuiteVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <InputMethodEvaluationSuite/PublicHeader.h>


