//
//  SessionResultsTableViewDataSource.swift
//  InputMethodEvaluationSuite
//
//  Created by Marcel Hansemann on 26/04/2017.
//  Copyright © 2017 Marcel Hansemann. All rights reserved.
//
// Convenience class - can be used to display the session results in the connected UITableView.
// Uses one table section per round.

import UIKit
/// A table view data source that can be used to show results after a session has been completed. Note: The session must have been completed for results to be correct.
public class SessionResultsTableViewDataSource : NSObject, UITableViewDataSource {
    @IBOutlet var cell : UITableViewCell?
    private var controller : SessionController {
        return SessionController.sharedInstance
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        guard controller.session != nil/*, controller.session.state == .cancelled || controller.session.state.finished*/ else { return 0 }
        return controller.roundResultCaches?.count ?? 0
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         guard controller.session != nil/*, controller.session.state == .cancelled || controller.session.state.finished*/ else { return 0 }
        return RoundMetrics.supportedKeys.count
    }
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Round \(section+1)"
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*guard cell != nil else { return UITableViewCell() }
        
        var returnCell : UITableViewCell!
        if let reuseIdentifier = self.cell?.reuseIdentifier, let theCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)  {
                returnCell = theCell
            
        }
        else {
            returnCell = self.cell!
        }
        */
        var returnCell : UITableViewCell
        
        if let existingCell = tableView.dequeueReusableCell(withIdentifier:  "DataCell") {
            returnCell = existingCell
            
        }
        else {
            class DataCell: UITableViewCell {
                
                override init(style style: UITableViewCellStyle, reuseIdentifier reuseIdentifier: String?) {
                    super.init(style: .value1, reuseIdentifier: reuseIdentifier)
                }
                
                required init?(coder aDecoder: NSCoder) {
                    super.init(coder: aDecoder)
                }
            }
            
            tableView.register(DataCell.self, forCellReuseIdentifier: "DataCell")
            returnCell = tableView.dequeueReusableCell(withIdentifier:  "DataCell")!
            
        }
        // The following two lines are for performance reasons (http://stackoverflow.com/questions/18460655/uicollectionview-scrolling-choppy-when-loading-cells)
        returnCell.layer.shouldRasterize = true
        returnCell.layer.rasterizationScale = UIScreen.main.scale
        
        let roundNumber = indexPath.section
        if let keyValue = controller.roundResultCaches?[roundNumber][indexPath.row] {
            returnCell.textLabel?.text = keyValue.key.rawValue.localized
            
            if let value = keyValue.value as? Double {
                returnCell.detailTextLabel?.text = String(format: "%.3f", value)
            }
            else {
                returnCell.detailTextLabel?.text = String(describing: keyValue.value)
            }
            
        }
        
        
        return returnCell
    }
}
